FROM debian:latest
WORKDIR /build
RUN echo "deb [trusted=yes] http://dev.packages.vyos.net/repositories/current current main" > /etc/apt/sources.list.d/vyos.list
RUN apt update
RUN apt install dkms gcc git debhelper bc linux-headers-5.10.110-amd64-vyos -y
ADD ./build.sh /build.sh
CMD ["/bin/bash", "/build.sh"]