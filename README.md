# Vyos Realtalk 88x2bu

The [rtl88x2bu](https://github.com/cilynx/rtl88x2bu) driver prebuild fot the [VyOS](https://vyos.net) kernel.

## How to use
### Build the Docker Image
`docker build -t vyos-kernel-env .`

### Build the Driver
`docker run --name vyos-rtl88x2bu vyos-kernel-env`

### Copy the Driver out of the Container
`docker cp "vyos-rtl88x2bu:/build" .`

### Cleanup
`docker rm vyos-rtl88x2bu`

### Upload the Package to the VyOS Instance
You can use any method of transferring a file in Linux to get the .deb file to the VyOs Instance.
- scp
- usb-drive
- file-upload / cloud services
- ...

### Install the Driver
`dpkg -I rtl88x2bu*.deb`

### Reboot

