#!/bin/bash
git clone https://github.com/cilynx/rtl88x2bu
cd rtl88x2bu
dkms build . -k 5.10.110-amd64-vyos
dkms mkbmdeb rtl88x2bu/5.8.7.1 -k 5.10.110-amd64-vyos
cd ..
cp /var/lib/dkms/rtl88x2bu/*/bmdeb/*.deb .